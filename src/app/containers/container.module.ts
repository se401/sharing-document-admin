import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DatatableComponent} from './datatable/datatable.component';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {ToolBarComponent} from './tool-bar/tool-bar.component';
import {RouterModule} from '@angular/router';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { CurrencyConvertComponent } from './currency-convert/currency-convert.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    DatatableComponent,
    ToolBarComponent,
    ConfirmModalComponent,
    CurrencyConvertComponent,

  ],
  imports: [
    CommonModule,
    NgbPaginationModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    DatatableComponent,
    ToolBarComponent,
    CurrencyConvertComponent
  ]
})
export class ContainerModule {
}
