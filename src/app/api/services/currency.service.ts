import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {ApiResponse} from '../models/ApiResponse';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private url = environment.baseUrl + '/Util';


  constructor(private http: HttpClient) {
  }

  convertCurrency(sourceCurrency: string, amount: number): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(`${this.url}/convert-currency`, {sourceCurrency, amount});
  }
}
