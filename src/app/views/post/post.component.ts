import {Component, OnInit} from '@angular/core';
import {Columnitem} from '../../containers/datatable/columnitem';
import {Post} from '../../api/models/Post';
import {DestroyComponent} from '../../containers/destroy.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {PostService} from '../../api/services/post.service';
import {takeUntil} from 'rxjs/operators';
import {Pageable} from '../../api/models/dto/Pageable';
import {CreatePostComponent} from './create-post/create-post.component';
import {Filterable} from '../../api/models/dto/Filterable';
import {TabDirective} from 'ngx-bootstrap/tabs';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent extends DestroyComponent implements OnInit {

  posts: Post[] = [];
  column: Columnitem[] = [
    {
      fieldLabel: 'Title',
      fieldName: 'title',
      type: 'link',
      fieldLink: 'id',
      url: '/post',
      sortable: true,
    },
    {
      fieldLabel: 'Description',
      fieldName: 'description',
      type: 'text',
    },
    {
      fieldLabel: 'Category',
      fieldName: 'category',
      type: 'object',
    },
    {
      fieldLabel: 'Image',
      fieldName: 'backgroudUrl',
      type: 'image',
    }
  ];

  activeId = '1';
  selectedPost: Post;
  bsModalRef: BsModalRef;
  totalRecords: number = 0;

  page = 1;
  loading = false;

  constructor(private postService: PostService,
              private modalService: BsModalService) {
    super();
  }

  ngOnInit(): void {
    this.getListPosts(this.activeId);

  }

  open(data: any) {
    if (data) {
      this.selectedPost = data;
    }
    this.bsModalRef = this.modalService.show(CreatePostComponent, {
      class: 'modal-dialog-centered', initialState: {
        data: this.selectedPost
      }
    });
    this.bsModalRef.onHidden.subscribe(val => {
      this.getListPosts(this.activeId);
      this.selectedPost = null;
    });
  }

  getListPosts(status): void {
    this.loading = true;
    const pageable: Pageable = {
      page: this.page, pageSize: 10, sortField: 'CreatedDate', sortOrder: 'desc',
    };
    const filterable: Filterable = {
      filterField: 'Status',
      filterValue: status || '1',
      filterOperator: 'eq'
    };
    this.postService.getActivePosts(pageable, filterable).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.posts = value.payload;
        this.totalRecords = value.paging.total;
        this.loading = false;
      },
      error => this.loading = false
    );
  }

  handleSuccess(data): void {
    this.getListPosts(this.activeId);
  }

  handlePageChange(value): void {
    this.page = value;
    this.getListPosts(this.activeId);
  }

  handleTabChange(event: TabDirective) {
    this.activeId = event.id;
    this.getListPosts(this.activeId);
  }

}
