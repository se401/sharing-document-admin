import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LevelService} from '../../../api/services/level.service';
import {DestroyComponent} from '../../../containers/destroy.component';
import {takeUntil} from 'rxjs/operators';
import {Level} from '../../../api/models/Level';
import {BsModalRef} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-level',
  templateUrl: './create-level.component.html',
  styleUrls: ['./create-level.component.scss']
})
export class CreateLevelComponent extends DestroyComponent implements OnInit {
  createForm: FormGroup;
  loading = false;
  data: Level;

  @Output() success: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private bsModalRef: BsModalRef,
              private levelService: LevelService) {
    super();
  }

  ngOnInit(): void {
    this.createForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['']
    });

    if (this.data) {
      this.f.name.patchValue(this.data.name);
      this.f.description.patchValue(this.data.description);
    }
  }

  get f() {
    return this.createForm.controls;
  }

  isFieldInvalid(f): boolean {
    return f.invalid && (f.dirty || f.touched);
  }

  onSubmit(): void {
    if (this.createForm.invalid) {
      return;
    }

    this.loading = true;

    const name = this.f.name.value;
    const description = this.f.description.value !== '' ? this.f.description.value : null;

    if (!this.data) {
      this.levelService.createLevel(name, description).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('createLevel', error);
          this.loading = false;
        }
      );
    } else {
      // update
      this.levelService.updateLevel(this.data.id, this.f.name.value, this.f.description.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('updateLevel', error);
          this.loading = false;
        }
      );
    }
  }

  onCLose(): void {
    this.bsModalRef.hide();
  }
}
