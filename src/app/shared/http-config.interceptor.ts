import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import jwtDecode from 'jwt-decode';
import {catchError} from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpConfigInterceptor {

  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('token') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjYwOTEwY2Y4NTcwM2JmMWM1ODdjNzgyYiIsIm5hbWVpZCI6IjYwOTEwY2Y4NTcwM2JmMWM1ODdjNzgyYiIsInJvbGUiOiJhZG1pbiIsInN1YiI6Im5ndXllbnRoYW5oY29uZzA4OEBnbWFpbC5jb20iLCJlbWFpbCI6Im5ndXllbnRoYW5oY29uZzA4OEBnbWFpbC5jb20iLCJuYmYiOjE2MjA3NTAyNDAsImV4cCI6MTYyMDc3MTg0MCwiaWF0IjoxNjIwNzUwMjQwfQ.HJdO-EBJE02i8QYKwOLbRWAE6YUuHTAv66mVnNvxH5A';
    if (token) {
      const decode = jwtDecode<any>(token);
      if (Date.now() > decode.exp * 1000) {
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
      }
      if (!request.headers.has('Authorization')) {
        request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token)});
      }
    }

    request = request.clone({headers: request.headers.set('Accept', 'Content-Type')});
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Origin', '*')});
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Credentials', 'true')});
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Methods', 'GET,POST')});
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept')});

    return next.handle(request).pipe(
      catchError((err) => {
        throw new Error(err.error.message);
      })
    );
  }
}
