export interface Filterable {
  filterField?: string;
  filterOperator?: string;
  filterValue?: string;
}
