import {Category} from './Category';
import {Level} from './Level';
import {Resource} from './Resource';
import {Account} from './Account';

export interface Post {
  id?: string;
  title: string;
  description: string;
  categoryId: string;
  levelId: string;
  resourceId: string;
  backgroudUrl: string;
  url: string;
  status: string;
  price: number;

  category?: Category;
  author?: Account;
  level?: Level;
  resource?: Resource;
}
