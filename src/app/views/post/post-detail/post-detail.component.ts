import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PostService} from '../../../api/services/post.service';
import {mergeMap, takeUntil} from 'rxjs/operators';
import {Post} from '../../../api/models/Post';
import {DestroyComponent} from '../../../containers/destroy.component';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent extends DestroyComponent implements OnInit {

  id: string;
  loading = true;
  post: Post;
  price = 0;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private postService: PostService) {
    super();
  }

  ngOnInit(): void {
    this.route.params
      .pipe(
        mergeMap(params => {
          this.id = params.id;
          return this.postService.getPostById(params.id);
        })
      )
      .subscribe(
        postRes => {
          if (postRes.statusCode === 200) {
            this.post = postRes.payload;
            this.price = postRes.payload.price;
            console.log('PostDetailComponent ~ ngOnInit ~ postRes.payload.price', postRes.payload.price);
          }
          this.loadSource();
          this.loading = false;
        },
        error => {
          console.log('-> getPostById error', error);
          this.loading = false;
        }
      );
  }

  getStatus(val: number): string {
    return val === 2 ? 'Active' : 'Pending';
  }

  loadSource(): void {
    this.postService.getUrl(this.id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        if (res.statusCode === 200) {
          this.post.url = res.payload;
        }
      }
    );
  }

  approvePost(): void {
    this.postService.approvePost(this.id, this.price).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      postRes => {
        if (postRes.statusCode === 200) {
          alert('Approve successfully');
          this.router.navigate(['/post/list']);
        }
      }
    );
  }

  handlePriceChange(price: string) {
    this.price = +price;
  }

}
