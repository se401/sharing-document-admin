export interface Base {
  id: string;
  createdDate: Date;
  lastModifiedDate: Date;
  isActive: boolean;
}
