import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Post} from '../models/Post';
import {Observable} from 'rxjs';
import {ApiResponse} from '../models/ApiResponse';
import {Pageable} from '../models/dto/Pageable';
import {Filterable} from '../models/dto/Filterable';
import {BuildFilter} from '../../shared/buildFilter';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url = environment.baseUrl + '/Post';

  constructor(private http: HttpClient) {
  }

  createPost(post: Post, background: File): Observable<ApiResponse<Post>> {
    const body = new FormData();
    body.append('Title', post.title);
    body.append('Description', post.description);
    body.append('CategoryId', post.categoryId);
    body.append('LevelId', post.levelId);
    body.append('ResourceId', post.resourceId);
    body.append('Background', background);
    body.append('Source', post.url);
    body.append('Price', post.price.toString());

    return this.http.post<ApiResponse<Post>>(`${this.url}/create-post`, body);
  }

  getPostById(id: string): Observable<ApiResponse<Post>> {
    return this.http.get<ApiResponse<Post>>(`${this.url}/get-post/${id}`);
  }

  getPostByCateId(cateId: string, pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);
    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-posts-cate/${cateId}`, {params});
  }

  getPostByResourceId(resourceId: string, pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);

    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-posts-resource/${resourceId}`, {params});
  }

  getPostByAuthorId(authorId: string, pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);
    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-posts-author/${authorId}`, {params});
  }

  getActivePosts(pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);
    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-active-posts`, {params});
  }

  getAll(pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);
    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-all-posts`, {params});
  }

  getApprovedPost(pageable: Pageable, filterable?: Filterable): Observable<ApiResponse<Post[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(),
      pageable.pageSize,
      pageable.page,
      pageable.sortField,
      pageable.sortOrder,
      filterable?.filterField,
      filterable?.filterOperator,
      filterable?.filterValue);
    return this.http.get<ApiResponse<Post[]>>(`${this.url}/get-appr-posts`, {params});
  }

  approvePost(postId: string, price: number): Observable<ApiResponse<Post>> {
    return this.http.post<ApiResponse<Post>>(`${this.url}/approve/${postId}`, {price: +(price).toFixed(5)});
  }

  editPost(id: string, post: Post, background?: File, source?: File): Observable<ApiResponse<Post>> {
    const body = new FormData();
    body.append('Title', post.title);
    body.append('Description', post.description);
    body.append('CategoryId', post.categoryId);
    body.append('LevelId', post.levelId);
    body.append('ResourceId', post.resourceId);
    body.append('Background', background);
    body.append('Source', source);
    return this.http.post<ApiResponse<Post>>(`${this.url}/edit/${id}`, body);
  }

  viewPost(id: string): Observable<ApiResponse<Post>> {
    return this.http.get<ApiResponse<Post>>(`${this.url}/view/${id}`);
  }

  getUrl(id: string):  Observable<ApiResponse<string>> {
    return this.http.get<ApiResponse<string>>(`${this.url}/get-url/${id}`);
  }
}
