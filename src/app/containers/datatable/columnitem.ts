export interface Columnitem {
  fieldLabel: string;
  fieldName: string;
  type: string;
  sortable?: boolean;
  fieldLink?: string;
  url?: string;
}
