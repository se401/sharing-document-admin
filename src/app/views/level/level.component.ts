import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Columnitem} from '../../containers/datatable/columnitem';
import {Level} from '../../api/models/Level';
import {DestroyComponent} from '../../containers/destroy.component';
import {LevelService} from '../../api/services/level.service';
import {takeUntil} from 'rxjs/operators';
import {CreateLevelComponent} from './create-level/create-level.component';
import {ConfirmModalComponent} from '../../containers/confirm-modal/confirm-modal.component';
import {EMPTY} from 'rxjs';

@Component({
  selector: 'app-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.scss']
})
export class LevelComponent extends DestroyComponent implements OnInit {
  levels: Level[] = [];
  total = 0;
  column: Columnitem[] = [
    {
      fieldLabel: 'Name',
      fieldName: 'name',
      type: 'text',
      sortable: true,
    },
    {
      fieldLabel: 'Description',
      fieldName: 'description',
      type: 'text',
      sortable: true,
    }
  ];

  selectedLevel: Level;
  bsModalRef: BsModalRef;

  constructor(private levelService: LevelService,
              private modalService: BsModalService) {
    super();
  }

  ngOnInit(): void {
    this.getList();
  }

  open(data: any) {
    if (data) {
      this.selectedLevel = data;
    }
    this.bsModalRef = this.modalService.show(CreateLevelComponent, {
      class: 'modal-dialog-centered', initialState: {
        data: this.selectedLevel
      }
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getList();
    });
  }

  getList(): void {
    this.levelService.getActiveLevel(10, 1, 'CreatedDate', 'desc').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.levels = value.payload;
        // tslint:disable-next-line:no-non-null-assertion
        this.total = value.paging!.total;
      }
    );
  }

  openDelete(): void {
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-dialog-centered', initialState: {
        observer$: EMPTY
      }
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getList();
    });
  }

}
