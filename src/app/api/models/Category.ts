import {Base} from './Base';

export interface Category extends Base {
  name: string;
  description: string;
  parentId: string;
  file: File;
}
