import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../models/Category';
import {environment} from '../../../environments/environment';
import {ApiResponse} from '../models/ApiResponse';
import {Level} from '../models/Level';
import {BuildFilter} from '../../shared/buildFilter';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private readonly http: HttpClient
  ) {
  }

  createCategory(name: string, description?: string, file?: File): Observable<ApiResponse<Category>> {
    const body = new FormData();
    body.append('Name', name);
    description && body.append('Description', description);
    file && body.append('file', name);
    return this.http.post<ApiResponse<Category>>(`${environment.baseUrl}/Category/create-category`, body);
  }

  updateCategory(id: string, name: string, description?: string, file?: File): Observable<ApiResponse<Category>> {
    const body = new FormData();
    body.append('Name', name);
    description && body.append('Description', description);
    file && body.append('file', name);
    return this.http.post<ApiResponse<Category>>(`${environment.baseUrl}/Category/update-category/${id}`, body);
  }

  getCategories(page: number, pageSize: number, sortField?: string, sortOrder?: string, filterField?: string,
                filterOperator?: string, filterValue?: string): Observable<ApiResponse<Category[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Category[]>>(`${environment.baseUrl}/Category/get-categories`, {params});
  }

  getActiveCategories(page: number, pageSize: number, sortField?: string, sortOrder?: string, filterField?: string,
                 filterOperator?: string, filterValue?: string): Observable<ApiResponse<Category[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Category[]>>(`${environment.baseUrl}/Category/get-active-categories`, {params});
  }
}
