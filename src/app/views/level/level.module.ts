import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LevelComponent} from './level.component';
import {RouterModule, Routes} from '@angular/router';
import {CreateLevelComponent} from './create-level/create-level.component';
import {ContainerModule} from '../../containers/container.module';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {path: '', component: LevelComponent}
];

@NgModule({
  declarations: [
    LevelComponent,
    CreateLevelComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ContainerModule,
    ModalModule,
    ReactiveFormsModule
  ],
  providers: [
    BsModalService
  ]
})
export class LevelModule {
}
