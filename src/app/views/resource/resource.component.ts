import {Component, OnInit} from '@angular/core';
import {DestroyComponent} from '../../containers/destroy.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {takeUntil} from 'rxjs/operators';
import {ResourceService} from '../../api/services/resource.service';
import {Columnitem} from '../../containers/datatable/columnitem';
import {Resource} from '../../api/models/Resource';
import {CreateResourceComponent} from './create-resource/create-resource.component';
import {ConfirmModalComponent} from '../../containers/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent extends DestroyComponent implements OnInit {
  resources: Resource[] = [];
  total = 0;
  column: Columnitem[] = [
    {
      fieldLabel: 'Name',
      fieldName: 'name',
      fieldLink: 'id',
      url: '/resource',
      type: 'link',
      sortable: true,
    },
    {
      fieldLabel: 'Description',
      fieldName: 'description',
      type: 'text',
      sortable: true,
    }
  ];

  selectedResource: Resource;
  bsModalRef: BsModalRef;

  constructor(private resourceService: ResourceService,
              private modalService: BsModalService) {
    super();
  }

  ngOnInit(): void {
    this.getList();
  }

  open(data: any) {
    if (data) {
      this.selectedResource = data;
    }

    this.bsModalRef = this.modalService.show(CreateResourceComponent, {
      class: 'modal-dialog-centered', initialState: {
        data: this.selectedResource
      }
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getList();
    });
  }

  getList(): void {
    this.resourceService.getActiveResource(10, 1, 'CreatedDate', 'desc').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.resources = value.payload;
        // tslint:disable-next-line:no-non-null-assertion
        this.total = value.paging!.total;
      }
    );
  }

  openDelete(): void {
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-dialog-centered'
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getList();
    });
  }
}
