import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResourceComponent} from './resource.component';
import {RouterModule, Routes} from '@angular/router';
import {CreateResourceComponent} from './create-resource/create-resource.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ContainerModule} from '../../containers/container.module';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';

const routes: Routes = [
  {path: '', component: ResourceComponent}
];

@NgModule({
  declarations: [
    ResourceComponent,
    CreateResourceComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ContainerModule,
    ModalModule
  ],
  providers: [
    BsModalService
  ]
})
export class ResourceModule {
}
