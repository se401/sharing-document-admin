FROM node:15.2.0 AS builder
RUN mkdir /app
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
# RUN npx ngcc --properties es2015 --create-ivy-entry-points
COPY . .
# RUN npm run ng build -- --prod --aot
RUN npm run build
FROM nginx:1.14.1-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
# COPY --from=builder /app/dist /usr/share/nginx/html
COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
