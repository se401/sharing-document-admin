import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Columnitem} from './columnitem';
import {Base} from '../../api/models/Base';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {
  page: number = 1;
  sortDir = null;
  sortField = '';

  selected: Base = null;

  @Input() data = [];
  @Input() totalRecords = 0;
  @Input() column: Columnitem[] = [];

  @Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() sortChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() openAddEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() openEditEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() openDeleteEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleSortChange(col: Columnitem) {
    this.sortField = col.fieldName;
    if (!this.sortDir) {
      this.sortDir = 'desc';
    } else {
      this.sortDir = this.sortDir === 'desc' ? 'asc' : 'desc';
    }

    this.sortField = col.fieldName;
    this.sortChange.emit({sortField: col.fieldName, sortDir: this.sortDir});
  }

  handleClick(item): void {
    this.selected = item;
  }

  openAdd(): void {
    this.openAddEvent.emit(null);
  }

  openEdit(): void {
    this.openEditEvent.emit(this.selected);
  }

  openDelete(): void {
    this.openDeleteEvent.emit(this.selected);
  }

  handlePageChange(value: number) {
    this.pageChange.emit(value);
  }
}
