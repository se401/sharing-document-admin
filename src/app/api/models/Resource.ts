import {Base} from './Base';

export interface Resource extends Base {
  name: string;
  description: string;
}
