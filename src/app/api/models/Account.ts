import {Base} from './Base';

export interface Account extends Base {
  email: string;
  username: string;
  phoneNumber: string;
  address: string;
  displayName: string;
  role: string;
  avatarUrl: string;
  bookmarks?: string;
}
