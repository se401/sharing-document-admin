import {HttpParams} from '@angular/common/http';

export enum FILTER_OPERATOR {
  'eq',
  'lt',
  'lte',
  'gt',
  'gte',
  'startswith',
  'contains'
}

export class BuildFilter {
  public static BuildSortParams(params: HttpParams, sortField?: string, sortOrder?: string): HttpParams {
    if (sortField) {
      params = params.set('SortField', sortField);
    }
    if (sortOrder) {
      params = params.set('SortOrder', sortOrder);
    }
    return params;
  }

  public static BuildPagingParams(params: HttpParams, pageSize: number, pageNumber: number): HttpParams {
    return params.set('PageSize', pageSize.toString()).set('PageNumber', pageNumber.toString());
  }

  public static BuildFilterParams(params: HttpParams, filterField: string,
                                  filterOperator: string,
                                  filterValue: string): HttpParams {
    if (filterField) {
      params = params.set('FilterField', filterField);
    }

    if (filterOperator) {
      params = params.set('FilterOperator', filterOperator);
    }

    if (filterValue) {
      params = params.set('FilterValue', filterValue);
    }
    return params;
  }

  public static buildFullParams(params: HttpParams, pageSize: number, pageNumber: number, sortField?: string, sortOrder?: string,
                                filterField?: string, filterOperator?: string,
                                filterValue?: string): HttpParams {
    params = params.set('PageSize', pageSize.toString()).set('PageNumber', pageNumber.toString());

    if (sortField) {
      params = params.set('SortField', sortField);
    }
    if (sortOrder) {
      params = params.set('SortOrder', sortOrder);
    }

    if (filterField) {
      params = params.set('FilterField', filterField);
    }

    if (filterOperator) {
      params = params.set('FilterOperator', filterOperator);
    }

    if (filterValue) {
      params = params.set('FilterValue', filterValue);
    }

    return params;
  }
}
