import {Component, EventEmitter, OnInit, Output, SkipSelf} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../containers/destroy.component';
import {Post} from '../../../api/models/Post';
import {PostService} from '../../../api/services/post.service';
import {Category} from '../../../api/models/Category';
import {Level} from '../../../api/models/Level';
import {Resource} from '../../../api/models/Resource';
import {CategoryService} from '../../../api/services/category.service';
import {ResourceService} from '../../../api/services/resource.service';
import {LevelService} from '../../../api/services/level.service';
import {CurrencyService} from '../../../api/services/currency.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent extends DestroyComponent implements OnInit {

  createForm: FormGroup;
  loading = false;
  data: Post;

  currency: string = 'VND';

  categories: Category[] = [];
  levels: Level[] = [];
  resources: Resource[] = [];

  file: File;
  source: File;

  @Output() success: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private bsModalRef: BsModalRef,
              private categoryService: CategoryService,
              private levelService: LevelService,
              private resourceService: ResourceService,
              private currencyService: CurrencyService,
              @SkipSelf() private postService: PostService) {
    super();
  }

  ngOnInit(): void {
    this.createForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      source: [null, [Validators.required, Validators.pattern(/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)]],
      file: [null],
      category: [null, [Validators.required]],
      price: [0, [Validators.required]],
      resource: [null, [Validators.required]],
      level: [null, [Validators.required]]
    });

    this.getCategories();
    this.getLevels();
    this.getResources();

    if (this.data) {
      this.f.name.patchValue(this.data.title);
      this.f.description.patchValue(this.data.description);
      this.f.level.patchValue(this.data.level);
      this.f.resource.patchValue(this.data.resource);
      this.f.category.patchValue(this.data.category);

      this.f.source.clearValidators();
    }
  }

  get f() {
    return this.createForm.controls;
  }

  compareWith(a: Post, b: Post): boolean {
    return a && b ? a.id === b.id : a === b;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      formGroup.updateValueAndValidity();
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  getCategories(): void {
    this.categoryService.getActiveCategories(0, 50).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        if (res.statusCode === 200) {
          this.categories = res.payload;
        }
      }
    );
  }

  getLevels(): void {
    this.levelService.getActiveLevel(50, 0).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        if (res.statusCode === 200) {
          this.levels = res.payload;
        }
      }
    );
  }

  getResources(): void {
    this.resourceService.getActiveResource(50, 0).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        if (res.statusCode === 200) {
          this.resources = res.payload;
        }
      }
    );
  }

  isFieldInvalid(f): boolean {
    return f.invalid && (f.dirty || f.touched);
  }

  onCLose(): void {
    this.createForm.reset();
    this.data = null;
    this.bsModalRef.onHidden.emit(true);
    this.bsModalRef.hide();
  }

  onSubmit(): void {

    if (this.createForm.invalid) {
      this.validateAllFormFields(this.createForm);
      return;
    }

    this.loading = true;

    const name = this.f.name.value;
    const source = this.f.source.value;
    const price = this.f.price.value;
    const description = this.f.description.value !== '' ? this.f.description.value : null;
    const categoryId = this.f.category.value !== '' ? this.f.category.value.id : null;
    const resourceId = this.f.resource.value !== '' ? this.f.resource.value.id : null;
    const levelId = this.f.level.value !== '' ? this.f.level.value.id : null;

    if (!this.data) {
      const post: Post = {
        title: name,
        description: description,
        backgroudUrl: '',
        categoryId: categoryId,
        url: source,
        levelId: levelId,
        resourceId: resourceId,
        status: '1',
        price: price
      };
      this.postService.createPost(post, this.createForm.controls.file.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('createPost', error);
          this.loading = false;
        }
      );
    } else {
      // update
      const newData: Post = {
        title: name,
        description: description,
        categoryId: categoryId,
        levelId: levelId,
        resourceId: resourceId,
        status: this.data.status,
        backgroudUrl: '',
        url: '',
        price: price
      };
      this.postService.editPost(this.data.id, newData, this.createForm.controls.file.value, this.createForm.controls.source.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('editPost', error);
          this.loading = false;
        }
      );

    }
  }

  handleSelectFile(event, source: string) {
    const fileVal = event.target.files as FileList;
    this.f[source].patchValue(fileVal.item(0));
    console.log(this.createForm.value);
  }

  handlePriceChange(price: string) {
    this.f.price.patchValue(+price);
  }

  handleCurrencyChange(currency: string) {
    this.currency = currency;
  }
}
