import {Component, OnInit} from '@angular/core';
import {Post} from '../../../api/models/Post';
import {Columnitem} from '../../../containers/datatable/columnitem';
import {PostService} from '../../../api/services/post.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Pageable} from '../../../api/models/dto/Pageable';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../containers/destroy.component';
import {Filterable} from '../../../api/models/dto/Filterable';
import {ConfirmModalComponent} from '../../../containers/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-approve-post',
  templateUrl: './approve-post.component.html',
  styleUrls: ['./approve-post.component.scss']
})
export class ApprovePostComponent extends DestroyComponent implements OnInit {
  posts: Post[] = [];
  column: Columnitem[] = [
    {
      fieldLabel: 'Title',
      fieldName: 'title',
      type: 'link',
      fieldLink: 'id',
      url: '/post',
      sortable: true,
    },
    {
      fieldLabel: 'Description',
      fieldName: 'description',
      type: 'text',
    },
    {
      fieldLabel: 'Image',
      fieldName: 'backgroudUrl',
      type: 'image',
    }
  ];

  selectedPost: Post;
  bsModalRef: BsModalRef;
  totalRecords: number = 0;

  constructor(private postService: PostService,
              private modalService: BsModalService) {
    super();
  }

  ngOnInit(): void {
    this.getListPosts();
  }

  open(data: any) {
    if (data) {
      this.selectedPost = data;
    }
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-dialog-centered', initialState: {
        observer$: this.postService.approvePost(this.selectedPost.id, this.selectedPost.price)
      }
    });
  }

  getListPosts(): void {
    const pageable: Pageable = {
      page: 1, pageSize: 10, sortField: 'CreatedDate', sortOrder: 'desc'
    };
    const filterable: Filterable = {
      filterField: 'Status',
      filterValue: '1',
      filterOperator: 'eq'
    };
    this.postService.getAll(pageable, filterable).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.posts = value.payload;
        this.totalRecords = value.paging.total;
      }
    );
  }

  handleSuccess(data): void {
    this.getListPosts();
  }

  handlePageChange(value): void {
    this.getListPosts();
  }

}
