import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import {DestroyComponent} from '../destroy.component';
import {CurrencyService} from '../../api/services/currency.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime, distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-currency-convert',
  templateUrl: './currency-convert.component.html',
  styleUrls: ['./currency-convert.component.scss']
})
export class CurrencyConvertComponent extends DestroyComponent implements OnInit {
  currency = 'VND';

  @Input() value = 0;

  @Output() priceChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() currencyChange: EventEmitter<any> = new EventEmitter<any>();

  currencyForm: FormGroup;

  constructor(private currencyService: CurrencyService,
              private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.currencyForm = this.fb.group({
      before_price: [0, [Validators.required]],
      price: [0, [Validators.required]]
    });

    this.f.before_price.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(value => this.currencyService.convertCurrency(this.currency, +value)),
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        if (value.statusCode === 200) {
          this.f.price.patchValue(value.payload.toFixed(5));
          this.priceChange.emit(value.payload.toFixed(5));
        }
      }
    );

    this.f.price.patchValue(this.value);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      formGroup.updateValueAndValidity();
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldInvalid(f): boolean {
    return f.invalid && (f.dirty || f.touched);
  }

  get f() {
    return this.currencyForm.controls;
  }

  handleChooseCurrency(event) {
    this.currency = event.target.value;
    this.currencyChange.emit(this.currency);
  }

}
