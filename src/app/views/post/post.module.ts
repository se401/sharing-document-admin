import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PostComponent} from './post.component';
import {RouterModule, Routes} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap/modal';
import {CreatePostComponent} from './create-post/create-post.component';
import {ContainerModule} from '../../containers/container.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ApprovePostComponent} from './approve-post/approve-post.component';
import {PostDetailComponent} from './post-detail/post-detail.component';
import {NgbToast, NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxLoadingModule } from 'ngx-loading';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Post'
    },
    children: [
      {
        path: '',
        redirectTo: 'list'
      },
      {path: 'list', component: PostComponent},
      {path: 'approve', component: ApprovePostComponent},
      {path: ':id', component: PostDetailComponent},
    ]
  },
];

@NgModule({
  declarations: [
    PostComponent,
    CreatePostComponent,
    ApprovePostComponent,
    PostDetailComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ContainerModule,
    ReactiveFormsModule,
    TabsModule,
    NgxLoadingModule.forRoot({}),
    FormsModule
  ],
  providers: [
    BsModalService
  ]
})
export class PostModule {
}
