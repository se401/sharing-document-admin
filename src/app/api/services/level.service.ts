import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Level} from '../models/Level';
import {environment} from '../../../environments/environment';
import {ApiResponse} from '../models/ApiResponse';
import {BuildFilter} from '../../shared/buildFilter';

@Injectable({
  providedIn: 'root'
})
export class LevelService {
  private url = environment.baseUrl + '/Level';

  constructor(private http: HttpClient) {
  }

  getAllLevels(pageSize: number, page: number, sortField?: string, sortOrder?: string, filterField?: string,
               filterOperator?: string, filterValue?: string): Observable<ApiResponse<Level[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Level[]>>(`${this.url}/get-levels`, {params});
  }

  getActiveLevel(pageSize: number, page: number, sortField?: string, sortOrder?: string, filterField?: string,
                 filterOperator?: string, filterValue?: string): Observable<ApiResponse<Level[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Level[]>>(`${this.url}/get-active-levels`, {params});
  }

  createLevel(name: string, description: string): Observable<ApiResponse<Level>> {
    return this.http.post<ApiResponse<Level>>(`${this.url}/create-level`, {Name: name, Description: description});
  }

  updateLevel(id: string, name: string, description: string): Observable<ApiResponse<Level>> {
    return this.http.post<ApiResponse<Level>>(`${this.url}/update-level/${id}`, {Name: name, Description: description});
  }
}
