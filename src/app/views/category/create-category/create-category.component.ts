import {Component, EventEmitter, OnInit, Output, SkipSelf} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../../api/services/category.service';
import {DestroyComponent} from '../../../containers/destroy.component';
import {takeUntil} from 'rxjs/operators';
import {Category} from '../../../api/models/Category';
import {BsModalRef} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent extends DestroyComponent implements OnInit {

  createForm: FormGroup;
  loading = false;
  data: Category;

  @Output() success: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private bsModalRef: BsModalRef,
              @SkipSelf() private categoryService: CategoryService) {
    super();
  }

  ngOnInit(): void {
    this.createForm = this.fb.group({
      name: ['', [Validators.required]],
      description: [''],
      file: [null]
    });

    if (this.data) {
      console.log('-> this.data', this.data);
      this.f.name.patchValue(this.data.name);
      this.f.description.patchValue(this.data.description);
    }
  }

  get f() {
    return this.createForm.controls;
  }

  isFieldInvalid(f): boolean {
    return f.invalid && (f.dirty || f.touched);
  }

  onFileChange(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.createForm.patchValue({
        file: file
      });
    }
  }

  onSubmit(): void {
    if (this.createForm.invalid) {
      return;
    }

    this.loading = true;

    const name = this.f.name.value;
    const description = this.f.description.value !== '' ? this.f.description.value : null;
    const file = this.f.file.value;

    if (!this.data) {
      this.categoryService.createCategory(name, description, file).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('createCategory', error);
          this.loading = false;

        }
      );
    } else {
      // update
      this.categoryService.updateCategory(this.data.id, this.f.name.value, this.f.description.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();

        },
        error => {
          console.log('updateCategory', error);
          this.loading = false;
        }
      );
    }
  }

  onCLose(): void {
    this.bsModalRef.hide();
  }
}
