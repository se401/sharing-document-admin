import {Component, EventEmitter, OnInit, Output, SkipSelf} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {ResourceService} from '../../../api/services/resource.service';
import {DestroyComponent} from '../../../containers/destroy.component';
import {takeUntil} from 'rxjs/operators';
import {Resource} from '../../../api/models/Resource';

@Component({
  selector: 'app-create-resource',
  templateUrl: './create-resource.component.html',
  styleUrls: ['./create-resource.component.scss']
})
export class CreateResourceComponent extends DestroyComponent implements OnInit {

  createForm: FormGroup;
  loading = false;
  data: Resource;

  @Output() success: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private bsModalRef: BsModalRef,
              @SkipSelf() private resourceService: ResourceService) {
    super();
  }

  ngOnInit(): void {
    this.createForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['']
    });

    if (this.data) {
      this.f.name.patchValue(this.data.name);
      this.f.description.patchValue(this.data.description);
    }
  }

  get f() {
    return this.createForm.controls;
  }

  isFieldInvalid(f): boolean {
    return f.invalid && (f.dirty || f.touched);
  }

  onCLose(): void {
    this.bsModalRef.hide();
  }

  onSubmit(): void {
    if (this.createForm.invalid) {
      return;
    }

    this.loading = true;

    const name = this.f.name.value;
    const description = this.f.description.value !== '' ? this.f.description.value : null;

    if (!this.data) {
      this.resourceService.createResource(name, description).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('createLevel', error);
          this.loading = false;
        }
      );
    } else {
      // update
      this.resourceService.updateResource(this.data.id, this.f.name.value, this.f.description.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        value => {
          console.log('-> value', value);
          this.loading = false;
          this.success.emit(value);
          this.onCLose();
        },
        error => {
          console.log('updateLevel', error);
          this.loading = false;
        }
      );
    }
  }
}
