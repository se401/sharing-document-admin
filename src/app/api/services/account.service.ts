import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Account} from '../models/Account';
import {Observable} from 'rxjs';
import {ApiResponse} from '../models/ApiResponse';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  url = environment.baseUrl + '/User';

  constructor(private http: HttpClient) {
  }

  updateAccountInfo(info: Account): Observable<ApiResponse<Account>> {
    return this.http.post<ApiResponse<Account>>(`${this.url}/update-account-info`, info);
  }

  changeAvatar(file: File): Observable<ApiResponse<Account>> {
    const body = new FormData();
    body.set('file', file);
    return this.http.post<ApiResponse<Account>>(`${this.url}/change-avatar`, body);
  }

  changePassword(password: string, confirmPassword: string): Observable<ApiResponse<Account>> {
    return this.http.post<ApiResponse<Account>>(`${this.url}/change-password`, {password, confirmPassword});
  }

  getUsers(): Observable<ApiResponse<Account[]>> {
    return this.http.get<ApiResponse<Account[]>>(`${this.url}/get-users`);
  }

  getUserById(id: string): Observable<ApiResponse<Account[]>> {
    return this.http.get<ApiResponse<Account[]>>(`${this.url}/get-user/${id}`);
  }

  getUserInfo(): Observable<ApiResponse<Account[]>> {
    return this.http.get<ApiResponse<Account[]>>(`${this.url}/get-user-info`);
  }

}
