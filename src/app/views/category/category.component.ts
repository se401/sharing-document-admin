import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {CategoryService} from '../../api/services/category.service';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../containers/destroy.component';
import {Category} from '../../api/models/Category';
import {Columnitem} from '../../containers/datatable/columnitem';
import {CreateCategoryComponent} from './create-category/create-category.component';
import {ConfirmModalComponent} from '../../containers/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends DestroyComponent implements OnInit {

  categories: Category[] = [];
  total = 0;
  column: Columnitem[] = [
    {
      fieldLabel: 'Name',
      fieldName: 'name',
      type: 'text',
      sortable: true,
    },
    {
      fieldLabel: 'Description',
      fieldName: 'description',
      type: 'text',
      sortable: true,
    }
  ];

  selectedCategory: Category;
  bsModalRef: BsModalRef;

  constructor(private categoryService: CategoryService,
              private modalService: BsModalService) {
    super();
  }

  ngOnInit(): void {
    this.getListCategory();
  }

  open(data: any) {
    if (data) {
      this.selectedCategory = data;
    }
    this.bsModalRef = this.modalService.show(CreateCategoryComponent, {
      class: 'modal-dialog-centered', initialState: {
        data: this.selectedCategory
      }
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getListCategory();
    });
  }

  openDelete(): void {
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-dialog-centered'
    });
    this.bsModalRef.onHide.subscribe((e) => {
      this.getListCategory();
    });
  }

  getListCategory(): void {
    this.categoryService.getActiveCategories(1, 10, 'CreatedDate', 'desc').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.categories = value.payload;
        // tslint:disable-next-line:no-non-null-assertion
        this.total = value.paging!.total;
      }
    );
  }

  handleSuccess(data): void {
    this.getListCategory();
  }

  handlePageChange(value): void {
    console.log('-> value', value);
    this.getListCategory();
  }
}
