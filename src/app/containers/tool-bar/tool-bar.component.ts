import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolBarComponent implements OnInit {

  @Input() disable = false;

  @Output() addEvt: EventEmitter<any> = new EventEmitter<any>();
  @Output() editEvt: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteEvt: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
  }

  add(): void {
    this.addEvt.emit(true);
  }

  edit(): void {
    this.editEvt.emit(true);
  }

  delete(): void {
    this.deleteEvt.emit(true);
  }
}
