import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiResponse} from '../models/ApiResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = environment.baseUrl + '/Auth';

  constructor(private http: HttpClient) {
  }

  login(email: string, password: string): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(`${this.url}/login`, {email, password});
  }
}
