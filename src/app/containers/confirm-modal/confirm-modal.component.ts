import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Observable} from 'rxjs';
import {DestroyComponent} from '../destroy.component';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent extends DestroyComponent implements OnInit {

  observer$: Observable<any>;

  constructor(private bsModalRef: BsModalRef) {
    super();
  }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.bsModalRef.hide();
  }

  onConfirm(): void {
    this.observer$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      value => {
        this.onCancel();
      }
    );
  }

}
