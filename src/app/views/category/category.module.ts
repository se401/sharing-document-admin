import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryComponent} from './category.component';
import {RouterModule, Routes} from '@angular/router';
import {ContainerModule} from '../../containers/container.module';
import {CreateCategoryComponent} from './create-category/create-category.component';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';

const routes: Routes = [
  {path: '', component: CategoryComponent}
];

@NgModule({
  declarations: [
    CategoryComponent,
    CreateCategoryComponent
  ],
  imports: [
    CommonModule,
    ContainerModule,
    RouterModule.forChild(routes),
    NgbDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule
  ],
  exports: [
    CreateCategoryComponent
  ],
  providers: [
    BsModalService
  ]
})
export class CategoryModule {
}
