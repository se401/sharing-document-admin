import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiResponse} from '../models/ApiResponse';
import {Resource} from '../models/Resource';
import {environment} from '../../../environments/environment';
import {BuildFilter} from '../../shared/buildFilter';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  private url = environment.baseUrl + '/Resource';

  constructor(private http: HttpClient) {
  }

  getAllResources(pageSize: number, page: number, sortField?: string, sortOrder?: string, filterField?: string,
                  filterOperator?: string, filterValue?: string): Observable<ApiResponse<Resource[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Resource[]>>(`${this.url}/get-resources`, {params});
  }

  getActiveResource(pageSize: number, page: number, sortField?: string, sortOrder?: string, filterField?: string,
                    filterOperator?: string, filterValue?: string): Observable<ApiResponse<Resource[]>> {
    const params = BuildFilter.buildFullParams(new HttpParams(), pageSize, page, sortField, sortOrder, filterField, filterOperator, filterValue);
    return this.http.get<ApiResponse<Resource[]>>(`${this.url}/get-active-resources`, {params});
  }

  createResource(name: string, description: string): Observable<ApiResponse<Resource>> {
    return this.http.post<ApiResponse<Resource>>(`${this.url}/create-resource`, {Name: name, Description: description});
  }

  updateResource(id: string, name: string, description: string): Observable<ApiResponse<Resource>> {
    return this.http.post<ApiResponse<Resource>>(`${this.url}/update-resource/${id}`, {Name: name, Description: description});
  }
}
