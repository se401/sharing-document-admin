import {Base} from './Base';

export interface Level extends Base {
  name: string;
  description: string;
}
