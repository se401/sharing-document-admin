export interface ApiResponse<T> {
  statusCode: number;
  message: string;
  payload: T;
  paging?: Paging;
}

export interface Paging {
  pageNumber: number;
  pageSize: number;
  total: number;
  totalPages: number;
}
